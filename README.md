## Objective
1. Explore bus stop data of San Francisco
2. Vizualize the bustop data and employee address on a map
3. Provide insights from visualization


## Data Sources
“Potential_Bust_Stops”: the list of potential bus stops. It is a list of intersections in the city. Columns:

• “Street_One”: one of two streets intersecting

• “Street_Two”: the other street intersecting

“Employee_Addresses”: the list of home address of each employee that in interested in taking buses. Columns:

• “address”: employee address

• “employee_id”: employee id, unique by employee



## [Click here for PYTHON CODE](Bus_Stop-Employees.ipynb)


## [Click here for Markdown](Bus_Stop-Employees.md)



## Map 
#### Bus Route
![](Bus_map.JPG "Image Title")


#### Bus Route - Employee Address
![](Bus_Map_Employee.JPG "Image Title")
